import datetime

__version__ = "1.0.4"

print('Date:', datetime.datetime.now().strftime('%A %b %d. %Y'))
print('Using ce_employment version:', __version__)
