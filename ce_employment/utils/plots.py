import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
import os


def plot_matrix(z,
                show=False, save=False, title=False,
                colorbar=False, log=False,
                fig_name="Unknown", path='figures', figsize=(15, 15),
                lim=100,
                **kwargs):
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    plt.set_cmap('PiYG')

    if log:
        norm = colors.LogNorm(z.mean().mean() + 0.5 * z.mean().std(), z.max().max(), clip='True')
        plot = plt.imshow(z, norm=norm, **kwargs)
    else:
        plot = plt.imshow(z, **kwargs)

    cap_y = z.sum().max() - lim * z.sum().std()
    inx_y = z.sum() >= cap_y

    cap_x = z.sum(axis=1).max() - lim * z.sum(axis=1).std()
    inx_x = z.sum(axis=1) >= cap_x

    ax.set_xticks(np.arange(len(z.columns))[inx_y])
    ax.set_yticks(np.arange(len(z.index))[inx_x])
    ax.set_xticklabels(z.columns[inx_y])
    ax.set_yticklabels(z.index[inx_x])

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    if title:
        plt.title(title)
    if colorbar:
        plt.colorbar(plot)
    plt.tight_layout()

    if show:
        plt.show()
    if save:
        if not os.path.exists(path):
            os.makedirs(path)
        plt.savefig("{}/{}.png".format(path, fig_name))
