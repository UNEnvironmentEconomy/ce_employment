import warnings
import numpy as np
import pandas as pd
import pymrio


def inv(x):
    """
    Returns diagflat inverse
    """

    if (type(x) is pd.DataFrame) or (type(x) is pd.Series):
        x = x.values
    if (type(x) is not np.ndarray) and (x == 0):
        recix = 0

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        recix = 1 / x
    recix[recix == np.inf] = 0
    recix = recix.reshape((1, -1))

    return np.diagflat(recix)


def downscale_Z(Z, E):
    return Z.mul(E, axis=0)


def downscale_Y(Y, E):
    return Y.mul(E)


def iot_exp(Z, Y, E):
    """(3) Expand IO

    1. Broadcast our vector to the Z shape

    2. Like an identity matrix but per group

    +---+------+------+------+
    |   | A    | B    | C    |
    +===+======+======+======+
    | A | 3107 | 0    | 0    |
    +---+------+------+------+
    | A | 3107 | 0    | 0    |
    +---+------+------+------+
    | A | 3107 | 0    | 0    |
    +---+------+------+------+
    | B | 0    | 1863 | 0    |
    +---+------+------+------+
    | B | 0    | 1863 | 0    |
    +---+------+------+------+
    | B | 0    | 1863 | 0    |
    +---+------+------+------+
    | C | 0    | 0    | 7345 |
    +---+------+------+------+
    | C | 0    | 0    | 7345 |
    +---+------+------+------+
    | C | 0    | 0    | 7345 |
    +---+------+------+------+

    """
    print(Z.shape)

    exp = pd.concat(
        [E['empl_p']]*len(Z),
        axis=1, keys=Z.columns
    )
    exp.index = E.index

    exp_left = exp.apply(
        lambda x: [x[e] if i in x.name else 0 for e, i in enumerate(x.index)],
        axis=0)

    Z = Z.fillna(0)

    Zr = exp_left @ Z @ exp_left.T
    Zr.index, Zr.columns = E["io_code"], E["io_code"]
    Zr = Zr.fillna(0)

    Y = Y.fillna(0)
    Yr = exp_left @ Y
    Yr.index = E["io_code"]

    return Zr, Yr
