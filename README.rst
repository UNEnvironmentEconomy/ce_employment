=============================
How to reproduce the results?
=============================

Clone the repository
======================

.. code-block:: bash

  git clone git@gitlab.com:UNEnvironmentEconomy/ce_employment.git

  cd ce_employment

Data
======================

**Employment data**
Because of licence of the input dataset we are not allowed to upload the require input datasets to the repository.
Pleae drop us a line to provide you with the required data-sets.

**IO data**
We utilise 2013 eora data which can be downloaded [here](https://www.worldmrio.com/eora26/). Store this data in your ce_employment folder in a new folder called ./data/eora

**Results**
Create a folder called ./tmp in your ce_employment folder where the results will be stored

Create a python virtual environment
======================================

.. code-block:: bash

   virtualenv --python=python3.9 env

Enter the virtual environment and install requirements with pip
================================================================

.. code-block:: bash

  source env/bin/activate

  pip install -r requirements.txt

Run the main script
===================

.. code-block:: bash

  python main.py

You should see the following output
===================================

.. code-block:: bash

  Date: Wednesday Dec 29. 2021
  Using ce_employment version: 1.0.4
  Load EORA data                	@ 15.79

  --------VERBOSE--------

  computing BRA/recife with method: cba
  	(1) bottom up data           	get local data
  /home/esteban/workspace/ce_employment/ce_employment/./data/empl_rec.csv
  @ 0.01
  	(2) Downscale IO             	@ 0.00
  	(3) Expand IO                	(26, 26)
  @ 0.49
  	(4) Index IO                 	@ 0.00
  	(5) A, L matrix              	@ 0.00
  	(6) MID                      	@ 0.30
  	(7) Apply Mask               	got mask
  @ 0.08
  	(8) Results
  @ 0.00
  ==============================
           empl       cjobs
  io_code
  1.1.I    2029  223.340325
  1.2.I      94   10.346964
  1.3.I     434   47.772154
  1.5.I     291   32.031560
  1.6.I     553   60.870971
  ------------------------------
  empl     878430.000000
  cjobs     87518.593046
  dtype: float64
  ==============================
  BRA-Recife - 9.96% Circular
  ==============================



  computing BRA/brussels with method: cba
  	(1) bottom up data           	get local data
  /home/esteban/workspace/ce_employment/ce_employment/./data/empl_bru.csv
  @ 0.01
  	(2) Downscale IO             	@ 0.00
  	(3) Expand IO                	(26, 26)
  @ 0.67
  	(4) Index IO                 	@ 0.00
  	(5) A, L matrix              	@ 0.01
  	(6) MID                      	@ 0.30
  	(7) Apply Mask               	got mask
  @ 0.19
  	(8) Results
  @ 0.00
  ==============================
           empl     cjobs
  io_code
  111.I      27  2.393947
  113.I      34  3.014600
  119.I       9  0.797982
  123.I       3  0.265994
  128.I       3  0.265994
  ------------------------------
  empl     714200.000000
  cjobs    108413.816164
  dtype: float64
  ==============================
  BRA-Brussels - 15.18% Circular
  ==============================



  computing BEL/recife with method: cba
  	(1) bottom up data           	get local data
  /home/esteban/workspace/ce_employment/ce_employment/./data/empl_rec.csv
  @ 0.00
  	(2) Downscale IO             	@ 0.00
  	(3) Expand IO                	(26, 26)
  @ 0.99
  	(4) Index IO                 	@ 0.00
  	(5) A, L matrix              	@ 0.01
  	(6) MID                      	@ 0.29
  	(7) Apply Mask               	got mask
  @ 0.08
  	(8) Results
  @ 0.00
  ==============================
           empl       cjobs
  io_code
  1.1.I    2029  157.077306
  1.2.I      94    7.277115
  1.3.I     434   33.598596
  1.5.I     291   22.528091
  1.6.I     553   42.811114
  ------------------------------
  empl     878430.000000
  cjobs     74112.126142
  dtype: float64
  ==============================
  BEL-Recife - 8.44% Circular
  ==============================



  computing BEL/brussels with method: cba
  	(1) bottom up data           	get local data
  /home/esteban/workspace/ce_employment/ce_employment/./data/empl_bru.csv
  @ 0.01
  	(2) Downscale IO             	@ 0.00
  	(3) Expand IO                	(26, 26)
  @ 0.60
  	(4) Index IO                 	@ 0.00
  	(5) A, L matrix              	@ 0.01
  	(6) MID                      	@ 0.29
  	(7) Apply Mask               	got mask
  @ 0.18
  	(8) Results
  @ 0.00
  ==============================
           empl     cjobs
  io_code
  111.I      27  1.693410
  113.I      34  2.132442
  119.I       9  0.564470
  123.I       3  0.188157
  128.I       3  0.188157
  ------------------------------
  empl     714200.000000
  cjobs     60756.736875
  dtype: float64
  ==============================
  BEL-Brussels - 8.51% Circular
  ==============================




  FINAL TIME: 22.48
